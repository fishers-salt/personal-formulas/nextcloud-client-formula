# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch', 'fedora'
    %w[nextcloud-client]
  else
    %w[nextcloud-desktop nextcloud-desktop-cmd nextcloud-desktop-l10n]
  end

packages.each do |pkg|
  control "nextcloud-client-package-clean-#{pkg}-pkg-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
