# frozen_string_literal: true

control 'nextcloud-client-config-clean-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/Nextcloud/nextcloud.cfg') do
    it { should_not exist }
  end
end

control 'nextcloud-client-config-clean-sync-list-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/Nextcloud/sync-exclude.lst') do
    it { should_not exist }
  end
end

control 'nextcloud-client-config-clean-config-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/Nextcloud') do
    it { should_not exist }
  end
end

control 'nextcloud-client-config-file-example-auser-dir-managed' do
  title 'should still exist so no data is lost'

  describe directory('/home/auser/nextcloud/example') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end
