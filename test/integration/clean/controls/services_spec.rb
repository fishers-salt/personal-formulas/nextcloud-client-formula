# frozen_string_literal: true

control 'nextcloud-client-service-clean-autostart-file-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/autostart/nextcloud.desktop') do
    it { should_not exist }
  end
end
