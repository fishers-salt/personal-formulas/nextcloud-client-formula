# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch', 'fedora'
    %w[nextcloud-client]
  else
    %w[nextcloud-desktop nextcloud-desktop-cmd nextcloud-desktop-l10n]
  end

packages.each do |pkg|
  control "nextcloud-client-package-install-#{pkg}-pkg-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
