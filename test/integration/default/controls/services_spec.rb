# frozen_string_literal: true

control 'nextcloud-client-service-running-autostart-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/autostart') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'nextcloud-client-service-running-autostart-file-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/autostart/nextcloud.desktop') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('[Desktop Entry]') }
    its('content') { should include('Name=Nextcloud') }
    its('content') { should include('GenericName=File Synchronizer') }
  end
end

control 'nextcloud-service-running-i3-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'nextcloud-service-running-i3-config-autostart-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/i3/autostart.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'nextcloud-service-running-i3-config-autostart-script-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/i3/autostart.d/nextcloud') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('/usr/bin/nextcloud --background') }
  end
end
