# frozen_string_literal: true

control 'nextcloud-client-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'nextcloud-client-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/Nextcloud') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'nextcloud-client-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/Nextcloud/nextcloud.cfg') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('[General]') }
    its('content') { should include('confirmExternalStorage = True') }
    its('content') { should include('newBigFolderSizeLimit = 3000') }
    its('content') { should include('optionalDesktopNotifications = True') }
    its('content') { should include('optionalServerNotifications = True') }
    its('content') { should include('useNewBigFolderSizeLimit = False') }
    its('content') { should include('[BWLimit]') }
    its('content') { should include('useDownloadLimit = -1') }
    its('content') { should include('useUploadLimit = -1') }
  end
end

control 'nextcloud-client-config-file-sync-list-auser-managed' do
  title 'should match desired lines'
  describe file('/home/auser/.config/Nextcloud/sync-exclude.lst') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('My Saved Places.') }
    its('content') { should include('downloads/incoming/*') }
  end
end

control 'nextcloud-client-config-file-example-auser-dir-managed' do
  title 'should exist'

  describe directory('/home/auser/nextcloud/example') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end
