# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_file = tplroot ~ '.config.file' %}
{%- from tplroot ~ "/map.jinja" import mapdata as nextcloud_client with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_file }}

{% if salt['pillar.get']('nextcloud-client-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_nextcloud-client', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

nextcloud-client-service-running-autostart-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/autostart
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - sls: {{ sls_config_file }}

nextcloud-client-service-running-autostart-file-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/autostart/nextcloud.desktop
    - source: {{ files_switch([
                  name ~ '-nextcloud.desktop.tmpl',
                  'nextcloud.desktop.tmpl'],
                lookup='nextcloud-client-service-running-autostart-file-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - nextcloud-client-service-running-autostart-dir-{{ name }}-managed

nextcloud-service-running-i3-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - sls: {{ sls_config_file }}

nextcloud-service-running-i3-config-autostart-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/i3/autostart.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - nextcloud-service-running-i3-config-dir-{{ name }}-managed

nextcloud-service-running-i3-config-autostart-script-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/i3/autostart.d/nextcloud
    - source: {{ files_switch([
                   name ~ '-nextcloud-autostart.sh.tmpl', 'nextcloud-autostart.sh.tmpl'],
                 lookup='nextcloud-service-running-i3-config-autostart-script-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - nextcloud-service-running-i3-config-autostart-dir-{{ name }}-managed
{% endif %}
{% endfor %}
{% endif %}
