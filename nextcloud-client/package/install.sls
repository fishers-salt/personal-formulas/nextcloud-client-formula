# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as nextcloud_client with context %}

nextcloud-client-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ nextcloud_client.pkgs }}
