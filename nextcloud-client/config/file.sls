# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as nextcloud_client with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('nextcloud-client-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_nextcloud-client', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

nextcloud-client-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

nextcloud-client-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/Nextcloud
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - nextcloud-client-config-file-user-{{ name }}-present

nextcloud-client-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0644'
    - replace: False
    - require:
      - nextcloud-client-config-file-config-dir-{{ name }}-managed

nextcloud-client-config-file-config-file-options-{{ name }}-managed:
  ini.options_present:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - sections:
        General:
          confirmExternalStorage: true
          newBigFolderSizeLimit: 3000
          optionalDesktopNotifications: true
          optionalServerNotifications: true
          useNewBigFolderSizeLimit: false
        BWLimit:
          useDownloadLimit: -1
          useUploadLimit: -1
    - require:
      - nextcloud-client-config-file-config-{{ name }}-managed

nextcloud-client-config-file-sync-list-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/Nextcloud/sync-exclude.lst
    - source: {{ files_switch([
                  name ~ 'sync-exclude.lst.tmpl',
                  'sync-exclude.lst.tmpl'],
                lookup='nextcloud-client-config-file-sync-list-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - nextcloud-client-config-file-config-dir-{{ name }}-managed

nextcloud-client-config-file-data-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/nextcloud
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - nextcloud-client-config-file-user-{{ name }}-present

{%- set nextcloud_user_data = user.get('nextcloud', {}) %}
{% if nextcloud_user_data != {} %}
{%- set accounts = nextcloud_user_data.get('accounts', {}) %}
{%- for account, details in accounts.items() %}
nextcloud-client-config-file-{{ account }}-{{ name }}-dir-managed:
  file.directory:
    - name: {{ home }}/nextcloud/{{ account }}
    - mode: '0755'
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - nextcloud-client-config-file-data-dir-{{ name }}-managed
{% endfor %}
{% endif %}

{% endif %}
{% endfor %}
{% endif %}
