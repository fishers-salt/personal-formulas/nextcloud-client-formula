# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_clean = tplroot ~ '.service.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as nextcloud_client with context %}

include:
  - {{ sls_service_clean }}

{% if salt['pillar.get']('nextcloud-client-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_nextcloud-client', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

nextcloud-client-config-clean-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg

nextcloud-client-config-clean-sync-list-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/Nextcloud/sync-exclude.lst

nextcloud-client-config-clean-config-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/Nextcloud

{% endif %}
{% endfor %}
{% endif %}
